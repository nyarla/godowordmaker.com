#!/usr/bin/perl

use strict;
use warnings;
use utf8;

package GodowordMaker;

use Plack::Request;
use Acme::GodoWord;
use Encode;
use Net::Twitter::Lite;

use base qw( Class::Data::Inheritable );

__PACKAGE__->mk_classdata( $_ )
    for qw( config ngword twitter enc );

__PACKAGE__->enc( Encode::find_encoding('utf-8') );

sub env_value {
    my ( $prefix, $key ) = @_;

    for ( $prefix, $key )
        { $_ = uc $_ }
    
    my $env = "${prefix}_${key}";

    if ( exists $ENV{$env} ) {
        return $ENV{$env};
    }

    return undef;
}

sub setup {
    my ( $class ) = @_;

    $class->setup_config;
    $class->setup_ngword;
    $class->setup_twitter;

    return 1;
}

sub setup_config {
    my ( $class ) = @_;
    my $file = $class->env_value('config');

    die "Configuration file is not file or readable: ${file}"
        if ( ! -e $file || ! -r $file );

    local $@;
    my $config = do $file;
    
    die "Failed to load configuration file: ${file}: ${@}"
        if ( $@ );

    die "Configuration data is not HASH reference: ${file}: ${config}"
        if ( ref $config ne 'HASH' );

    $class->config($config);

    return 1;
}

sub setup_ngword {
    my ( $class ) = @_;

    $class->ngword( $class->config->{'ngword'} );

    return 1;
}

sub setup_twitter {
    my ( $class ) = @_;

    my $config  = $class->config->{'twitter'} || {};
    my $twitter = Net::Twitter::Lite->new( %{ $config } );

    $class->twitter( $twitter );

    return 1;
}

sub to_app {
    my ( $class ) = @_;

    $class->setup;
    
    my $enc     = $class->enc;

    return sub {
        my $req = Plack::Request->new(shift);
        my $res = $req->new_response(200);
        
        my $message = $enc->decode( $req->parameters->{'message'} );
        
        if ( $message =~ m{^\s*$} ) {
            $res->status(500);
            $res->body('Source message is empty.');
        }
        elsif ( ! $class->has_ngword($message) ) {
            my $godoword    = $class->godoword($message);
            my $result      = eval { $class->twitter->update("${godoword} #godoword") };
            
            if ( $@ ) {
                $res->status(500);
                $res->body("Post to twitter is failed.");
            }
            else {
                $res->status(200);
                $res->body('Post to twitter is success.');
            }
        }
        else {
            $res->status(403);
            $res->body('Posted message has ngword.');
        }
        
        return $res->finalize;
    };
}

sub has_ngword {
    my ( $class, $message ) = @_;

    for my $ngword ( @{ $class->ngword } ) {
        return 1 if ( $message =~ m{$ngword} );
    }

    return 0;
}

sub godoword {
    my ( $class, $message ) = @_;

    my @tokens  = Acme::GodoWord->tokenize($message);
    for ( @tokens )
        { $_ =~ s{\s*}{}g; }
    
    @tokens = grep { $_ ne q{} } @tokens;

    return q{【} . join(q{」「}, @tokens) . q{】};
}

