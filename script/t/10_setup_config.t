#!perl

use strict;
use warnings;

use Test::More;
use t::Util qw( $rootdir $script );

BEGIN { require $script }

local $ENV{'GODOWORDMAKER_CONFIG'} = "${rootdir}/t/examples/config.pl";

GodowordMaker->setup_config();

is_deeply(
    GodowordMaker->config,
    { foo => 'bar' },
);

done_testing;
