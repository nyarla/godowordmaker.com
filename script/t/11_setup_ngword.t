#!perl

use strict;
use warnings;

use Test::More;
use t::Util qw( $rootdir $script );

BEGIN { require $script }

GodowordMaker->config({ ngword => [qw( foo bar baz )] });

GodowordMaker->setup_ngword();

is_deeply(
    GodowordMaker->ngword,
    [qw( foo bar baz )],
);

done_testing;
