#!perl

use strict;
use warnings;

use Test::More;
use t::Util qw( $script );

BEGIN { require $script }

local $ENV{'GODOWORDMAKER_FOO'} = 'bar';

is( GodowordMaker->env_value('foo'), 'bar' );

done_testing;
