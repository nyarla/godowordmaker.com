#!perl

use strict;
use warnings;

use Test::More;
use t::Util qw( $rootdir $script );

BEGIN { require $script }

GodowordMaker->ngword([qw( foo bar baz )]);

ok( ! GodowordMaker->has_ngword('aaa') );

ok( GodowordMaker->has_ngword('foo') );

done_testing;
