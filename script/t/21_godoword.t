#!perl

use strict;
use warnings;
use utf8;

use Test::More;
use t::Util qw( $rootdir $script );

BEGIN { require $script }

is(
    GodowordMaker->godoword('あなたには見えない'),
    q{【あなた」「には」「見えない】},
);

done_testing;
