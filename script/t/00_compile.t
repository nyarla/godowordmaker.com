#!perl

use strict;
use warnings;

use Test::More tests => 1;
use t::Util qw( $script );

BEGIN { require_ok($script) }
