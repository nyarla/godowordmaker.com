package t::Util;

use strict;
use warnings;

use base qw( Exporter );

use File::Spec;
use FindBin;

our ( $rootdir, $script );
our @EXPORT_OK = qw( $rootdir $script );

{
    my @path = File::Spec->splitdir( $FindBin::Bin );

    while ( my $dir = pop @path ) {
        if ( $dir eq 't' ) {
            $rootdir = File::Spec->catfile( @path );
            $script  = File::Spec->catfile( @path, 'app.pl' );
        }
    }
}

1;
