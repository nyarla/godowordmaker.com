#!perl

use strict;
use warnings;

use Test::More;
use t::Util qw( $rootdir $script );

use Plack::Test;
use HTTP::Request;
use Encode;

BEGIN { require $script }

{
    package DummyTwitter;
    
    sub new { bless {}, shift }
    
    our $message;
    
    sub update {
        my $self    = shift;
        $message = shift;
        
        return 1;
    }

    package FailedTwitter;
    
    sub new { bless {}, shift }
    
    sub update { die }
}

local $ENV{'GODOWORDMAKER_CONFIG'} = "${rootdir}/t/examples/app.pl";

my $app = GodowordMaker->to_app();
GodowordMaker->twitter( DummyTwitter->new );

# ok
test_psgi(
    app     => $app,
    client  => sub {
        my $cb = shift;
        my $req = HTTP::Request->new( GET => 'http://localhost/?message=あなたには見えない' );
        my $res = $cb->( $req );
        
        is( $res->code, 200 );
        is( $DummyTwitter::message, Encode::decode( 'utf-8', q{【あなた」「には」「見えない】 #godoword} ) );
        is( $res->content, 'Post to twitter is success.' );
    },
);

# message empty
test_psgi(
    app     => $app,
    client  => sub {
        my $cb = shift;
        my $req = HTTP::Request->new( GET => 'http://localhost/?message=' );
        my $res = $cb->( $req );
        
        is( $res->code, 500 );
        is( $res->content, 'Source message is empty.' );
    },
);

# post failed
GodowordMaker->twitter( FailedTwitter->new );
test_psgi(
    app     => $app,
    client  => sub {
        my $cb = shift;
        my $req = HTTP::Request->new( GET => 'http://localhost/?message=ほぐえ' );
        my $res = $cb->( $req );
        
        is( $res->code, 500 );
        like( $res->content, qr{Post to twitter is failed:} );
    },
);

# ngword
GodowordMaker->twitter( DummyTwitter->new );
test_psgi(
    app     => $app,
    client  => sub {
        my $cb = shift;
        my $req = HTTP::Request->new( GET => 'http://localhost/?message=うんこ' );
        my $res = $cb->( $req );
        
        is( $res->code, 403 );
        is( $res->content, 'Posted message has ngword.' );
    },
);

done_testing;
