#!perl

use strict;
use warnings;

use Test::More;
use t::Util qw( $rootdir $script );

BEGIN { require $script }

GodowordMaker->config({ twitter => {} });

GodowordMaker->setup_twitter;

isa_ok( GodowordMaker->twitter, 'Net::Twitter::Lite' );

done_testing;
