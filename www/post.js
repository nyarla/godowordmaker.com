$(function () {
    $('button#submit').click(function () {
        var result  = $('p#result');
        var message = $('textarea#message');
        
        result.html('');
        
        if ( message.val() == '' ) {
            result.html('つぶやいてほしい内容が空ですよ。');
        }
        else {
            result.html('Twitterへ投稿中です。しばらくお待ちいただけますか？　黒桐さん。');
            $.ajax({
                type:       'POST',
                url:        '/post.cgi',
                dataType:   'text',
                data:       {
                    'message':  message.val(),
                },
                error:      function ( xhr, status, thrown ) {
                    var code = xhr.status;
                    if( code == 500 ) {
                        result.html('Twitterへの投稿に失敗しました。通信環境になにか問題があるのでしょうか。');
                    }
                    else if ( code == 403 ) {
                        result.html('教師としてつぶやくのに相応しからぬ単語が含まれているようです。妙なことを言わせようとしないでください。黒桐さん。');
                    }
                },
                success:    function ( data, status, xhr ) {
                    result.html('Twitterへの投稿に成功したようですね。黒桐さん。');
                    message.val('');
                }
            });
        }
    });
});
