#!/usr/bin/perl

use strict;
use warnings;

use lib         '/home/nyarla/local/perl5/lib/perl5';
use local::lib  '~/local/perl5';

use Plack::Loader;

my $rootdir     = '/home/nyarla/script/godowordmaker.com';

local $ENV{uc('godowordmaker_config')} = "${rootdir}/config/config.pl";

require "${rootdir}/app/script/app.pl";

Plack::Loader->load('CGI')->run( GodowordMaker->to_app );
